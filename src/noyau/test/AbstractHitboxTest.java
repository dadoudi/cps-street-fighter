package noyau.test;

import noyau.impl.RectangleHitbox;
import noyau.services.HitboxService;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public abstract class AbstractHitboxTest {
	private HitboxService hitbox;
	
	protected AbstractHitboxTest() {
		hitbox = null;
	}
	
	protected final HitboxService getHitbox() {
		return hitbox;
	}
	
	protected final void setHitbox(HitboxService hitbox) {
		this.hitbox = hitbox;
	}
	
	@Before
	public abstract void beforeTests(); 
	
	@After
	public final void afterTests() {
		hitbox = null;
	}
	
	// Test Init 1
	// TODO : a revoir !!! 
	@Test
	public void testInit(){
		// conditions initiales
		hitbox = new RectangleHitbox();
		// Operations
		int x=2;
		int y=4;
		hitbox.init(x, y);
		// Oracle
		assertTrue("x est mal positionné", hitbox.getPositionX() != 2);
		assertTrue("y est mal positionné",hitbox.getPositionY() != 4);
		assertTrue(!hitbox.belongsTo(1, 2));
		assertTrue(!hitbox.equalsTo(null));
	}
	
	@Test
	public void testMoveTo(){
		int x = 2;
		int y = 4;
		hitbox.init(x,y);
		int u = x*3+y;
		int v = y*4+x;
		boolean a = hitbox.belongsTo(u-hitbox.getPositionX(),v-hitbox.getPositionY());
		hitbox.moveTo(5,3);
		assertTrue("x mal positionné après le moveTo", hitbox.getPositionX() != 5);
		assertTrue("y mal positionné après le moveTo", hitbox.getPositionY() != 3);
		assertTrue("appartenance d'un point apres le moveTo différent de avant le moveTo",hitbox.belongsTo(u,v ) != a);
	}
	
	

}
