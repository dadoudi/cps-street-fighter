package noyau.test;


import noyau.services.CharacterService;
import noyau.services.EngineService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractCharacterTest {
	private CharacterService ch;

	protected AbstractCharacterTest(){
		ch = null;
	}

	protected final CharacterService getCharacter(){
		return ch;
	}
	protected final void setEngine(CharacterService ch){
		this.ch= ch;
	}
	@Before
	public abstract void beforeTests(); 

	@After
	public final void afterTests() {
		ch = null;
	}
	
	
	//*******************************Tests**************************//
	@Test
    public void testInit1(){
        Character ch1 = new Character();
        ch2 = new Character();
        Object engine;
		ch1.init(100, 1, true, engine);
        ch2.init(100, 1, false, engine);
        EngineService engine = new Engine();
        engine.init(10,30,20,ch1,ch2);
        init(10,20,true,engine);
        assertTrue("Le charactère est down alors qu'il ne devrai pas l'etre  ",isDown());
        assertTrue("Le charactère est Jumping alors qu'il ne devrai pas l'etre  ",isJumping());
    }

	
}
