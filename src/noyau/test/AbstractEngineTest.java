package noyau.test;
import static org.junit.Assert.assertTrue;



import noyau.commande.Commande;
import noyau.impl.Engine;
import noyau.impl.Character;
import noyau.services.CharacterService;
import noyau.services.EngineService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractEngineTest {
	private EngineService engine;
	private CharacterService ch1;
	private CharacterService ch2;

	protected AbstractEngineTest(){
		engine = null;
	}

	protected final EngineService getEngine(){
		return engine;
	}
	protected final void setEngine(EngineService engine){
		this.engine = engine;
	}
	@Before
	public abstract void beforeTests(); 

	@After
	public final void afterTests() {
		engine = null;
	}

	//***********************Test*************************************//
	@Test
	public void testInit1(){
		//conditionS initiales
		engine = new Engine();
		ch1 = new Character();
		ch2 = new Character();
		ch1.init(100, 1, true, engine);
		ch2.init(100, 1, false, engine);
		engine.init(10,30,20,ch1,ch2);
		//tester post conditions
		assertTrue("Engine n'a pas la bonne hauteur",engine.getHeight() != 10);
		assertTrue("Engine n'a pas la bonne largeur",engine.getWidth() != 30);
		assertTrue("charactere 1 n'est pas bien attribué a engine",!engine.getChar(1).equals(ch1));
		assertTrue("charactere 2 n'est pas bien attribué a engine",!engine.getChar(2).equals(ch1));
		assertTrue("position x de caractère 1 != w/2 -s/2",engine.getChar(1).getPositionX() != 30/2 - 20/2);
		assertTrue("position x de caractère2 != w/2 -s/2",engine.getChar(2).getPositionX() != 30/2 + 20/2);
		assertTrue("position y de caractère !=0",engine.getChar(1).getPositionY() != 0);
		assertTrue("position y de caractère !=0",engine.getChar(1).getPositionY() != 0);
		assertTrue("character 1 de engine n'est pas en faceRight",engine.getChar(1).faceRight() == false);
		assertTrue("character 2 de engine est en faceRight",engine.getChar(2).faceRight());
		
	}
	

	@Test
	public void testStep(){
		//conditionS initialeS
		engine = new Engine();
		ch1 = new Character();//not dead 
		ch2 = new Character();// not dead 
		ch1.init(100,10,true,engine);
		ch2.init(100,10,false,engine);
		//le fait de faire des init sur les charactères assurent que gameOver est faux car les deux charactères ne sont pas dead 
		engine.init(10,30,20,ch1,ch2);
		//capture 
		Character ch3 = new Character();//not dead 
		Character ch4 = new Character();// not dead 
		ch3.init(100,10,true,engine);
		ch4.init(100,10,false,engine);
		ch3.step(Commande.RIGHT);
		//ch3 == ch1 ET ch4==ch2 
		engine.step(Commande.RIGHT, Commande.NEUTRAL);
		ch4.step(Commande.NEUTRAL);
		//tester les post conditions 
		assertTrue("le caractère 1 n'a pas la meme position X avec step et commande ",engine.getChar(1).getPositionX() != (ch3.getPositionX()));
		assertTrue("le caractère 2 n'a pas la meme position X avec step et commande ",engine.getChar(2).getPositionX()!=(ch4.getPositionX()));
	}
	

}
