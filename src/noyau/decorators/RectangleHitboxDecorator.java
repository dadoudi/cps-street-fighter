package noyau.decorators;

import noyau.services.RectangleHitboxService;

public abstract class RectangleHitboxDecorator extends HitboxDecorator implements RectangleHitboxService {

	
	protected RectangleHitboxDecorator(RectangleHitboxService delegate) {
		super((RectangleHitboxService)delegate);
	}
	
	public void init(int x, int y, int h, int l) {
		((RectangleHitboxService)delegate).init(x, y, h, l);
	}
	
	public int getHauteur() {
		return ((RectangleHitboxService)delegate).getHauteur();
	}
	
	public int getLargeur() {
		return ((RectangleHitboxService)delegate).getLargeur();
	}
	
}
