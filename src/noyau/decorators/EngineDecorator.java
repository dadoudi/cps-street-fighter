package noyau.decorators;

import noyau.commande.Commande;
import noyau.services.CharacterService;
import noyau.services.EngineService;

public abstract class EngineDecorator implements EngineService {
	private EngineService delegate;

	public EngineDecorator (EngineService delegate){
		this.delegate = delegate;
	}
	
	public int getHeight(){
		return delegate.getHeight();
	}
	public int getWidth(){
		return delegate.getWidth();
	}
	
	public CharacterService getChar(int i){
		return delegate.getChar(i);
	}
	public CharacterService getPlayer(int i){
		return delegate.getPlayer(i);
	}
	public boolean gameOver(){
		return delegate.gameOver();
	}
	
	public void init(int h, int w, int s, CharacterService p1, CharacterService p2){
		delegate.init(h, w, s, p1, p2);
	}
	
	public void step(Commande c1, Commande c2){
		delegate.step(c1, c2);
	}
}
