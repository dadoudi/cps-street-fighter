package noyau.decorators;

import noyau.commande.Commande;
import noyau.services.CharacterService;
import noyau.services.EngineService;
import noyau.services.HitboxService;

public class CharacterDecorator implements CharacterService {
	protected CharacterService delegate; 
	
	public CharacterDecorator(CharacterService delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public int getPositionX() {
		return delegate.getPositionX();
	}

	@Override
	public int getPositionY() {
		return delegate.getPositionY();
	}

	@Override
	public EngineService getEngine() {
		return delegate.getEngine();
	}

	@Override
	public HitboxService getCharBox() {
		return delegate.getCharBox();
	}

	@Override
	public int getLife() {
		return delegate.getLife();
	}

	@Override
	public int getSpeed() {
		return delegate.getSpeed();
	}

	@Override
	public boolean faceRight() {
		return delegate.faceRight();
	}

	@Override
	public boolean dead() {
		return delegate.dead();
	}

	@Override
	public void init(int l, int s, boolean f, EngineService e) {
		delegate.init(l, s, f, e);
	}

	@Override
	public void moveLeft() {
		delegate.moveLeft();
	}

	@Override
	public void moveRight() {
		delegate.moveRight();
	}

	@Override
	public void switchSide() {
		delegate.switchSide();
	}

	@Override
	public void step(Commande c) {
		delegate.step(c);
	}
}
