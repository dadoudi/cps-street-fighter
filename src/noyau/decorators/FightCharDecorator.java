package noyau.decorators;

import noyau.services.CharacterService;
import noyau.services.FightCharService;
import noyau.services.TechData;

public abstract class FightCharDecorator extends CharacterDecorator implements FightCharService {

	public FightCharDecorator(CharacterService delegate) {
		super(delegate);
	}

	@Override
	public boolean isBlocking() {
		return ((FightCharService)delegate).isBlocking();
	}

	@Override
	public boolean isBlockstunned() {
		return ((FightCharService)delegate).isBlockstunned();
	}

	@Override
	public boolean isHitstunned() {
		return ((FightCharService)delegate).isHitstunned();
	}

	@Override
	public boolean isTeching() {
		return ((FightCharService)delegate).isTeching();
	}

	@Override
	public TechData getTech() {
		return ((FightCharService)delegate).getTech();
	}

	@Override
	public int techFrame() {
		return ((FightCharService)delegate).techFrame();
	}

	@Override
	public boolean techHasAlreadyHit() {
		return ((FightCharService)delegate).techHasAlreadyHit();
	}

	@Override
	public void startTech(TechData tech) {
		((FightCharService)delegate).startTech(tech);		
	}

}
