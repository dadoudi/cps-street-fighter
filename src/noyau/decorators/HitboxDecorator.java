package noyau.decorators;

import noyau.services.HitboxService;

public abstract class HitboxDecorator implements HitboxService {
	protected HitboxService delegate;
	
	protected HitboxDecorator(HitboxService delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public int getPositionX(){
		return delegate.getPositionX();
	}
	
	@Override
	public int getPositionY(){
		return delegate.getPositionY();
	}
	
	@Override
	public boolean belongsTo(int i, int j){
		return delegate.belongsTo(i, j);
	}
	
	@Override
	public boolean collidesWith(HitboxService h1){
		return delegate.collidesWith(h1);
	}
	
	@Override
	public boolean equalsTo(HitboxService H1){
		return delegate.equalsTo(H1);
	}
	
	@Override
	public void init(int x, int y){
		 delegate.init(x,y);
	}
	
	@Override
	public void moveTo(int x, int y){
		delegate.moveTo(x, y);
	}
}
