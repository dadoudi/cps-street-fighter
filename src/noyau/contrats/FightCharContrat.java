package noyau.contrats;

import noyau.commande.Commande;
import noyau.contrats.errors.InvariantError;
import noyau.contrats.errors.PostconditionError;
import noyau.contrats.errors.PreconditionError;
import noyau.decorators.FightCharDecorator;
import noyau.services.CharacterService;
import noyau.services.EngineService;
import noyau.services.HitboxService;
import noyau.services.TechData;

public class FightCharContrat extends FightCharDecorator {

	public FightCharContrat(CharacterService delegate) {
		super(delegate);
	}

	public void checkInvariant() {
		// \inv : (getPositionX() > 0) && getPositionX() < getEngine().getWidth()
		if( ! (getPositionX() > 0 && getPositionX() < getEngine().getWidth()) ) 
			throw new InvariantError("Character : PositionX est hors de l'arène");

		// \inv : (getPositionY() > 0) && getPositionY() < getEngine().getHeight()
		if( ! (getPositionY() > 0 && getPositionY() < getEngine().getHeight()) ) 
			throw new InvariantError("Character : PositionY est hors de l'arène");

		// \inv : dead() == !(getLife() > 0)
		if( ! (dead() == !(getLife() > 0)) ) 
			throw new InvariantError("Character : dead() == !(getLife() > 0)");

		// \inv : isTeching() == \exists t: Tech \in tech().equals(t)
		if( ! (isTeching() == !(getTech().equals(null))) )
			throw new InvariantError("Character : isTeching");
	}

	public void init(int l , int s, boolean f, EngineService e) {
		// PRE

		// \pre : l > 0
		if ( ! (l > 0) ) 
			throw new PreconditionError("Character : l > 0");

		// \pre : s > 0
		if ( ! (s > 0) ) 
			throw new PreconditionError("Character : s > 0");

		// Traitement
		super.init(l, s, f, e);

		// post-init invariant
		checkInvariant();

		// \post : getLife() == l && getSpeed() == s && faceRight() == f && getEngine().equals(e)
		if ( !( getLife() == l && getSpeed() == s && faceRight() == f && getEngine().equals(e)))
			throw new PostconditionError("Character init");

		// \post : \exists h : HitboxService \in {getCharBox().equals(h)}  
		HitboxService h1 = getEngine().getChar(1).getCharBox();
		HitboxService h2 = getEngine().getChar(2).getCharBox();

		if ( !( getCharBox().equals(h1) || getCharBox().equals(h2) ) ) 
			throw new PostconditionError("Character init");
	}

	public void startTech(TechData tech) {
		// PRE 

		// pre-invariant
		checkInvariant();

		// \pre : isTeching() == false
		if( isTeching() )
			throw new PreconditionError("startTech pre 1");

		// Traitement 
		super.startTech(tech);

		//post-invariant 
		checkInvariant();

		// \post : isTeching()
		if( !isTeching() )
			throw new PostconditionError("startTech post 1");
		
		// \post : getTech().equals(tech)
		if ( !getTech().equals(tech) )
			throw new PostconditionError("startTech post 2");
	}

	public void moveLeft() {
		// PRE : pas de préconditions 

		// pre-invariant
		checkInvariant();

		// Captures 
		// TODO : comment faire une capture d'objet ? 
		EngineService captureEngine = getEngine(); 
		int capturePositionX = getPositionX();
		int capturePositionY = getPositionY();
		int captureSpeed = getSpeed();
		boolean captureFace = faceRight();
		int captureLife = getLife();

		// Traitement 
		super.moveLeft();

		//post-invariant 
		checkInvariant();

		// \post : !( \exists i \in { !(getEngine()@pre.getChar(i).equals(this)) && getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox())} ) 
		// 		   || (getPositionX() == getPositionX()@pre) }

		CharacterService c1 = captureEngine.getChar(1);
		CharacterService c2 = captureEngine.getChar(2);

		boolean b1 = !(c1.equals(this)) && getCharBox().collidesWith(c1.getCharBox());
		boolean b2 = !(c2.equals(this)) && getCharBox().collidesWith(c2.getCharBox());

		if ( !( !b1 && !b2 && getPositionX() == capturePositionX ) )
			throw new PostconditionError("Character moveleft post 1");

		// \post : getPositionX()@pre <= getSpeed()@pre
		//         && !( \forall i \in ( getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox()))) ) 
		//         || getPositionX() == 0

		b1 = c1.equals(this) || !(getCharBox().collidesWith(c1.getCharBox()));
		b2 = c2.equals(this) || !(getCharBox().collidesWith(c2.getCharBox()));

		if ( ! (capturePositionX <= captureSpeed && !( b1 && b2 ) || getPositionX() == 0) )
			throw new PostconditionError("Character moveleft post 2");

		// \post : getPositionX()@pre > getSpeed()@pre
		//         && !(\forall i \in { getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox))})
		//		   || getPositionX() == getPositionX()@pre - getSpeed()@pre

		if ( !(capturePositionX > captureSpeed && !( b1 && b2 ) || (getPositionX() == capturePositionX - captureSpeed)) ) 
			throw new PostconditionError("Character moveleft post 3");

		// \post : faceRight() == faceRight()@pre && getLife() == getLife()@pre
		if ( !(faceRight() == captureFace && getLife() == captureLife) )
			throw new PostconditionError("Character moveleft post 4");

		// \post : getPositionY() == getPositionY()@pre
		if ( !(getPositionY() == capturePositionY) )
			throw new PostconditionError("Character moveleft post 5");
	}

	public void moveRight() {
		// PRE : pas de préconditions 

		// pre-invariant
		checkInvariant();

		// Captures 
		// TODO : comment faire une capture d'objet ? 
		EngineService captureEngine = getEngine(); 
		int capturePositionX = getPositionX();
		int capturePositionY = getPositionY();
		int captureSpeed = getSpeed();
		boolean captureFace = faceRight();
		int captureLife = getLife();

		// Traitement 
		super.moveRight();

		//post-invariant 
		checkInvariant();

		// \post : !( \exists i \in { !(getEngine()@pre.getChar(i).equals(this)) && getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox())} ) 
		// 		   || (getPositionX() == getPositionX()@pre) }

		CharacterService c1 = captureEngine.getChar(1);
		CharacterService c2 = captureEngine.getChar(2);

		boolean b1 = !(c1.equals(this)) && getCharBox().collidesWith(c1.getCharBox());
		boolean b2 = !(c2.equals(this)) && getCharBox().collidesWith(c2.getCharBox());

		if ( !( !b1 && !b2 && getPositionX() == capturePositionX ) )
			throw new PostconditionError("Character moveleft post 1");

		// \post : getPositionX()@pre <= getEngine().getWidth() - getSpeed()@pre
		//         && !( \forall i \in ( getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox()))) ) 
		//         || getPositionX() == getPositionX()@pre + getSpeed()@pre

		b1 = c1.equals(this) || !(getCharBox().collidesWith(c1.getCharBox()));
		b2 = c2.equals(this) || !(getCharBox().collidesWith(c2.getCharBox()));

		if ( ! (capturePositionX <= getEngine().getWidth() - captureSpeed && !( b1 && b2 ) || getPositionX() == capturePositionX + captureSpeed) )
			throw new PostconditionError("Character moveright post 2");

		// \post : getPositionX()@pre > getEngine().getWidth() - getSpeed()@pre
		//         && !(\forall i \in { getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox))})
		//		   || getPositionX() == getEngine().getWidth()

		if ( !(capturePositionX > getEngine().getWidth() - captureSpeed && !( b1 && b2 ) || (getPositionX() == getEngine().getWidth())) ) 
			throw new PostconditionError("Character moveleft post 3");

		// \post : faceRight() == faceRight()@pre && getLife() == getLife()@pre
		if ( !(faceRight() == captureFace && getLife() == captureLife) )
			throw new PostconditionError("Character moveleft post 4");

		// \post : getPositionY() == getPositionY()@pre
		if ( !(getPositionY() == capturePositionY) )
			throw new PostconditionError("Character moveleft post 5");
	}

	public void switchSide() {
		// PRE : pas de precondition

		// pre-invariant
		checkInvariant();

		// Captures 
		int capturePositionX = getPositionX();
		boolean captureFace = faceRight();

		// Traitement 
		super.switchSide();

		//post-invariant 
		checkInvariant();

		// \post : faceRight() != faceRight()@pre
		if ( !( faceRight() != captureFace) )
			throw new PostconditionError("Character switchSide post 1");

		// \post : getPositionX() == getPositionX()@pre
		if ( !( getPositionX() == capturePositionX) )
			throw new PostconditionError("Character switchSide post 1");
	}

	public void step(Commande c) {
		// PRE 

		// \pre : dead() == false
		if ( !dead() )
			throw new PreconditionError("Character step pre 1");

		// pre-invariant
		checkInvariant();

		// Traitement 
		super.step(c);

		//post-invariant 
		checkInvariant();

		// TODO : corriger les post conditions avant 
	}

}
