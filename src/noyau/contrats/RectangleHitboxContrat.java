package noyau.contrats;

import noyau.contrats.errors.InvariantError;
import noyau.contrats.errors.PostconditionError;
import noyau.decorators.RectangleHitboxDecorator;
import noyau.services.HitboxService;
import noyau.services.RectangleHitboxService;

public class RectangleHitboxContrat extends RectangleHitboxDecorator {

	private HitboxService h1;
	protected RectangleHitboxContrat(RectangleHitboxService delegate) {
		super(delegate);
	}

	private void checkInvariant() {
		// 1
		// \inv: collidesWith(H1) == {\exists x:int && y:int in\ belongsTo(x,y) && H1.belongsTo(x,y)} 

		boolean collides = false; 
		for(int x = 0; x<getLargeur();x++){
			for(int y = 0; y <getHauteur(); y++){
				if(belongsTo(x,y) && belongsTo(x,y)){
					collides = true; 

				}
				if( collides ){
					break;
				}
			}
		}


		// TODO ???? 
		if ( ! (collidesWith(h1) == collides)) {
			throw new InvariantError("Premier invariant non valide : collidesWith");
		}

		//2
		// \inv: equalsTo(H1) == { \forall x:int && y:int in\{ belongsTo(x,y) == H1.belongsTo(x,y)}}
		boolean equals = true;
		for(int x = 0; x<getLargeur();x++){
			for(int y = 0; y <getHauteur(); y++){
				if( (belongsTo(x,y) && ! belongsTo(x,y))  || ( !belongsTo(x,y) &&  belongsTo(x,y)) ){
					equals = false; 
				}
			}
		}
		/*int x = getPositionX() + 5;
		int y = getPositionY() - 2;
		if( ! (equalsTo(h1) == ( belongsTo(x,y) && h1.belongsTo(x, y)))) {
			throw new InvariantError("Second invariant non valide : equalsTo");
		}*/

		if(!(equalsTo(h1) == equals)){
			throw new InvariantError("Second invariant non valide : equalsTo");
		}

	}


	@Override
	public void init(int x, int y, int h, int l) {

		// PRE : pas de précondition
		checkInvariant();
		// Traitement

		super.init(x, y, h, l);

		// post-init invariant
		checkInvariant();


		// \post: getPositionX()==x
		if(getPositionX() != x){
			throw new PostconditionError("getPoxitionX != x");
		}
		// \post: getPositionY()==y
		if(getPositionX() != y){
			throw new PostconditionError("getPoxitionY != y");
		}

		// \post: getHauteur()==h
		if(! (getHauteur() == h) ){
			throw new PostconditionError("getHauteur != h ");
		}

		// \post: getLargeur()==l
		if(! (getLargeur() == l) ){
			throw new PostconditionError("getLargeur != l ");
		}
	}


}
