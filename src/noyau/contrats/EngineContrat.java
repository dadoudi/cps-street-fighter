package noyau.contrats;

import noyau.commande.Commande;
import noyau.contrats.errors.InvariantError;
import noyau.contrats.errors.PostconditionError;
import noyau.contrats.errors.PreconditionError;
import noyau.decorators.EngineDecorator;
import noyau.services.CharacterService;
import noyau.services.EngineService;

public abstract class EngineContrat extends EngineDecorator {

	public EngineContrat(EngineService delegate) {
		super(delegate);
	}

	public void checkInvariant(){
		// \inv : gameOver() == { \exists i \in {1,2} \in { getPlayer(i).getChar().dead() } }
		if(!((getChar(1).dead()) || getChar(2).dead()) && gameOver() ){
			throw new InvariantError("invariant gameOver non verifié");
		}
	}

	public void init(int h, int w, int s, CharacterService p1, CharacterService p2){
		// PRE
		
		// \pre : h > 0
		if( ! (h > 0) ) 
			throw new PreconditionError("Init : h > 0");

		// \pre : s > 0
		if( ! (s > 0) ) 
			throw new PreconditionError("Init : s > 0");
		
		// \pre : w > s
		if( ! (w > s) ) 
			throw new PreconditionError("Init : w > s");
		
		// \pre : p1.equals(p2) == false
		if( p1.equals(p2) ) 
			throw new PreconditionError("Init : !p1.equals(p2)");


		// Traitement
		super.init(h, w, s, p1, p2);

		// post-init invariant
		checkInvariant();

		// \post : getHeight() == h
		if(!(getHeight() == h)){
			throw new PostconditionError("post condition  getHeight() == h non respectée" );
		}
		// \post : getWidth() == w
		if(!(getWidth() == w)){
			throw new PostconditionError("post condition  getWidth() == w non respectée" );
		}
		// \post : getChar(1).equals(p1)
		if(!(getChar(1).equals(p1))){
			throw new PostconditionError("post condition  getChar(1).equals(p1) non respectée" );
		}
		// \post : getChar(2).equals(p2)
		if(!(getChar(2).equals(p2))){
			throw new PostconditionError("post condition  getChar(2).equals(p2) non respectée" );
		}
		// \post : getChar(1).getPositionX() == w//2 - s//2
		if(!(getChar(1).getPositionX() == getWidth()/2.0 - s/2.0)){
			throw new PostconditionError("post condition getChar(1).getPositionX() == w//2 - s//2  non respectée" );
		}
		// \post : getChar(2).getPositionX() == w//2 + s//2
		if(!(getChar(2).getPositionX() == getWidth()/2.0 + s/2.0)){
			throw new PostconditionError("post condition getChar(2).getPositionX() == w//2 + s//2 non respectée" );
		}
		// \post : getChar(1).getPositionY() == 0
		if(!(getChar(1).getPositionY() == 0)){
			throw new PostconditionError("post condition getChar(1).getPositionY() == 0  non respectée" );
		}
		// \post : getChar(2).getPositionY() == 0
		if(!(getChar(2).getPositionY() == 0)){
			throw new PostconditionError("post condition  getChar(2).getPositionY() == 0  non respectée" );
		}
		// \post : getChar(1).faceRight()
		if(!(getChar(1).faceRight())){
			throw new PostconditionError("post condition getChar(1).faceRight()  non respectée" );
		}
		// \post : getChar(2).faceRight() == false
		if((getChar(2).faceRight())){
			throw new PostconditionError("post condition getChar(2).faceRight()  non respectée" );
		}
	}

	public void step(Commande c1, Commande c2){
		// PRE
		// \pre : gameOver() == false
		if(gameOver()){
			throw new PreconditionError("la précondition gameOver() == false n'es pas vérifiée");
		}
		
		// pre-invariant
		checkInvariant();

		//CAPTURES
		// TODO Cloner ????? 
		CharacterService char1_at_pre = getChar(1);
		CharacterService char2_at_pre = getChar(2);
		// TRAITEMENT
		super.step(c1, c2);

		// post-invariant
		checkInvariant();
		
		// \post : getChar(1).equals(getChar(1)@pre.step(c1))
		// TODO : Comment tester ? Cloner l'obj avant ?? 
		// \post : getChar(2).equals(getChar(2)@pre.step(c2))
		// TODO Pareil ! 
	}
}
