package noyau.contrats;

import noyau.contrats.errors.PostconditionError;
import noyau.decorators.HitboxDecorator;
import noyau.services.HitboxService;

public class HitboxContrat extends HitboxDecorator {
	private HitboxService h1;

	protected HitboxContrat(HitboxService service) {
		super(service);
		h1.init(5, 6);
	}

	public void checkInvariant() {
		
		// \inv: collidesWith(H1) == {\exists x:int && y:int in\ belongsTo(x,y) && H1.belongsTo(x,y)} 
		
		// TODO ???? 
		if ( ! (collidesWith(h1) == ??)) {
			throw new InvariantError("Premier invariant non valide : collidesWith");
		}
		
		// \inv: equalsTo(H1) == { \forall x:int && y:int in\{ belongsTo(x,y) == H1.belongsTo(x,y)}}
		 	
		int x = getPositionX() + 5;
		int y = getPositionY() - 2;
		if( ! (equalsTo(h1) == ( belongsTo(x,y) && h1.belongsTo(x, y)))) {
			throw new InvariantError("Second invariant non valide : equalsTo");
		}
	}

	@Override
	public void init(int x, int y) {
		// PRE : pas de précondition
		checkInvariant();
		// Traitement
		super.init(x,y);

		// post-init invariant
		checkInvariant();
		// \post: getPositionX()==x
		if(getPositionX() != x){
			throw new PostconditionError("getPoxitionX != x");
		}
		// \post: getPositionY()==y
		if(getPositionX() != y){
			throw new PostconditionError("getPoxitionY != y");
		}

	}

	@Override
	public void moveTo(int x, int y) {
		// PRE : pas de précondition

		// pre-invariant
		checkInvariant();
		/* Capture du centre */
		boolean belongsTo_centre_at_pre = belongsTo(getPositionX(), getPositionY());
		/* Capture du centre + 100 */
		boolean belongsTo_centre_100_at_pre = belongsTo(getPositionX()+100, getPositionY()+100);
		/* Capture d'un point absolu */
		int getPositionX_at_pre = getPositionX();
		int getPositionY_at_pre = getPositionY();
		boolean belongsTo_abs_at_pre = belongsTo(300, 0);

		super.moveTo(x, y);
		checkInvariant();

		/* Test du centre */

		if( ! belongsTo(getPositionX(), getPositionY()) == belongsTo_centre_at_pre) 
			throw new PostconditionError("getPositionX()==x or getPositionX()==y");

		/* Test du centre + 100 */
		if( ! belongsTo(getPositionX() + 100, getPositionY() + 100) == belongsTo_centre_100_at_pre) 
			throw new PostconditionError("getPositionX()==x or getPositionX()==y");

		/* Test d'un point absolu */
		if( ! belongsTo(300 + (x - getPositionX_at_pre), y - getPositionY_at_pre) == belongsTo_abs_at_pre) 
			throw new PostconditionError("getPositionX()==x or getPositionX()==y");
	}
}
