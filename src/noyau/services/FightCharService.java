package noyau.services;

public interface FightCharService extends CharacterService {
	/* Observators */
	
	public boolean isBlocking(); // position defense
	public boolean isBlockstunned(); // a été stun pdt qu'il etait en defense
	public boolean isHitstunned(); // a ete stun pdt qu'il lancait une technique 
	public boolean isTeching();
	// \pre : isTeching()
	public TechData getTech();
	// \pre : isTeching()
	public int techFrame();
	// \pre : isTeching()
	public boolean techHasAlreadyHit();
	
	/*invariants*/
	// \inv : isTeching() == \exists t: Tech \in tech().equals(t)
	// \inv : 
	
	/* operators */
	
	// \pre : isTeching() == false
	// \post : isTeching() 
	// \post : getTech().equals(tech)
	public void startTech(TechData tech);
	
	// TODO : specif (voir rapport)
	public void getStunned(int blockstun);
	public void setBlocking(boolean b);

}
