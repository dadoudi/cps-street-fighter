package noyau.services;

import noyau.commande.Commande;

public interface EngineService {

	/* Observators */

	int getHeight();
	int getWidth();
	// \pre : i \in {1, 2}
	CharacterService getChar(int i);
	// \pre : i \in {1,2}
	CharacterService getPlayer(int i);
	boolean gameOver();

	/*invariants*/
	// \inv : gameOver() == { \exists i \in {1,2} \in { getChar(i).dead() } }
	
	/* constructors */
	
	// \pre : h > 0
	// \pre : s > 0
	// \pre : w > s
	// \pre : p1.equals(p2) == false
	// \post : getHeight() == h
	// \post : getWidth() == w
	// \post : getChar(1).equals(p1)
	// \post : getChar(2).equals(p2)
	// \post : getChar(1).getPositionX() == w//2 - s//2
	// \post : getChar(2).getPositionX() == w//2 + s//2
	// \post : getChar(1).getPositionY() == 0
	// \post : getChar(2).getPositionY() == 0
	// \post : getChar(1).faceRight() == true
	// \post : getChar(2).faceRight() == false
	public void init(int h, int w, int s, CharacterService p1, CharacterService p2);

	
	/* operators */
	// \pre : gameOver() == false
	// \post : getChar(1).equals(getChar(1)@pre.step(c1))
	// \post : getChar(2).equals(getChar(2)@pre.step(c2))
	public void step(Commande c1, Commande c2);
}
