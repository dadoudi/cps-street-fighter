package noyau.services;

public class TechData {
	public final int damage;
	public final int hitstun;
	public final int blockstun;
	public final int startupframe;
	public final int hitframe;
	public final int recoveryframe;
	public final HitboxService hitbox;
	// Sert a savoir a quelle hauteur la technique doit etre lancee : de 0 a 4
	public final int ratio;
	
	public TechData(int d, int hs, int bs, int sf, int hf, int rf, HitboxService hitbox,int ratio) {
		damage = d;
		hitstun = hs;
		blockstun = bs;
		startupframe = sf;
		hitframe = hf;
		recoveryframe = rf;
		this.hitbox = hitbox;
		this.ratio = ratio;
	}
	
}
