package noyau.services;

import noyau.commande.Commande;

public interface CharacterService {
	
	/* Observators */

	public int getPositionX();
	public int getPositionY();
	public EngineService getEngine();
	public HitboxService getCharBox();
	public int getLife();
	public int getSpeed();
	public boolean faceRight();
	public boolean dead ();
	public boolean isJumping();
	public boolean isDown();



	/*invariants*/
	// \inv : (getPositionX() > 0) && getPositionX() < getEngine().getWidth()
	// \inv : (getPositionY() > 0) && getPositionY() < getEngine().getHeight()
	// \inv : dead() == !(getLife() > 0)
	// \inv : !isDown() && !isJumping()

	/* constructors */

	// \pre : l > 0
	// \pre : s > 0
	// \post : getLife() == l && getSpeed() == s && faceRight() == f && getEngine().equals(e)
	// \post : \exists h : HitboxService \in {getCharBox().equals(h)}  
	// \post : !isDown()
	// \post : !isJumping()
	public void init(int l , int s, boolean f, EngineService e);

	/* operators */

	// \post : !( isJumping() || isDown() || \exists i \in { !(getEngine()@pre.getChar(i).equals(this)) && getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox())} ) 
	// 		   || (getPositionX() == getPositionX()@pre) }
	// \post : getPositionX()@pre < getSpeed()@pre
	//         && !( \forall i \in ( getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox()))) ) 
	//         || getPositionX() == 0
	// \post : getPositionX()@pre >= getSpeed()@pre
	//         && !(\forall i \in { getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox))})
	//		   || getPositionX() == getPositionX()@pre - getSpeed()@pre
	// \post : faceRight() == faceRight()@pre && getLife() == getLife()@pre
	// \post : getPositionY() == getPositionY()@pre
	public void moveLeft();

	// \post : !( isJumping() || isDown() || \exists i \in { !(getEngine()@pre.getChar(i).equals(this)) && getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox())} ) 
	// 		   || (getPositionX() == getPositionX()@pre) }
	// \post : getPositionX()@pre <= getEngine().getWidth() - getSpeed()@pre
	//         && !( \forall i \in ( getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox()))) ) 
	//         || getPositionX() == getPositionX()@pre + getSpeed()@pre
	// \post : getPositionX()@pre > getEngine().getWidth() - getSpeed()@pre
	//         && !(\forall i \in { getEngine()@pre.getChar(i).equals(this) || !( getCharBox().collisionWith(getEngine()@pre.getChar(i).getCharBox))})
	//		   || getPositionX() == getEngine().getWidth()
	// \post : faceRight() == faceRight()@pre && getLife() == getLife()@pre
	// \post : getPositionY() == getPositionY()@pre
	public void moveRight();

	// \post : faceRight() != faceRight()@pre
	// \post : getPositionX() == getPositionX()@pre
	public void switchSide();

	// \pre : dead() == false
	// \post : moveLeft().equals(step(LEFT)) 
	// \post : moveRight().equals(step(RIGHT))
	// \post : step(NEUTRAL).equals(this)
	// \post : step(UP).equals(moveTop())
	// \post : step(DOWN).equals(moveDown())
	public void step(Commande c);
	
	// \pre : damage > 0
	// \post : life() == life()@pre - damage
	public void getInjured(int damage);
	
	// \post : ! ( !isJumping()@pre && !isDown()@pre ) || isDown()
	// \post : ! ( !isJumping()@pre && isDown()@pre ) || !isDown()
	// \post : isJumping() == isJumping()@pre
	public void moveDown();
	
	// \post : ! ( !isJumping()@pre && !isDown()@pre ) || isJumping()
	public void moveTop();
	
}

