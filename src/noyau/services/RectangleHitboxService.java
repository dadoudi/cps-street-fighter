package noyau.services;

public interface RectangleHitboxService extends HitboxService {

	/* Observators */
	int getHauteur();
	int getLargeur();

	//Position X et Y : point correspondant au coin inférieur gauche

	/* constructors */
	// \pre: h > 0 && l > 0
	// \post: getPositionX()==x
	// \post: getPositionY()==y
	// \post: getHauteur()==h
	// \post: getLargeur()==l
	public void init(int x, int y, int h, int l);
}
