package noyau.services;

public interface HitboxService {
	
	/* Observators */
	public int getPositionX();
	public int getPositionY();
	public boolean belongsTo(int i, int j);
	public boolean collidesWith(HitboxService H1);
	public boolean equalsTo(HitboxService H1);
	
	//===========================================================================
	
	/*invariants*/
	// \inv: collidesWith(H1) == {\exists x:int && y:int in\ belongsTo(x,y) && H1.belongsTo(x,y)} 
	// \inv: equalsTo(H1) == { \forall x:int && y:int in\{ belongsTo(x,y) == H1.belongsTo(x,y)}}
	
	
	//===========================================================================
	
	/* constructors */
	// \post: getPositionX()==x
	// \post: getPositionY()==y
	public void init(int x, int y);
	
	//===========================================================================

	/* operators */
	// \post: getPositionX()==x
	// \post: getPositionX()==y
	// \post: \forall u:int && v:int in\{ belongsTo(u,v).equals(belongsTo(u-(x-getPositionX()@pre),v-(y-getPositionY()@pre))@pre)}
	public void moveTo(int x, int y); 
}
