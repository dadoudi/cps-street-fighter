package noyau.impl;

import noyau.commande.Commande;
import noyau.services.CharacterService;
import noyau.services.EngineService;
import noyau.services.HitboxService;
import noyau.services.RectangleHitboxService;

public class Character implements CharacterService {
	protected EngineService engine;
	protected HitboxService hitbox;
	protected int life;
	protected int speed;
	protected boolean faceRight;

	protected int hauteurNormale;
	protected int positionXNormale;

	// Compteurs pour gerer les sauts et accroupissements 

	protected boolean isJumping;
	protected final int jumpFrame = 6; // 6 frames pour sauter ;
	protected final int hauteurJump = 3;
	protected boolean isDown;
	protected int compteurJump;

	// ----------------- Accesseurs / Observateurs ----------------- //
	@Override
	public int getPositionX() {
		return hitbox.getPositionX();
	}

	@Override
	public int getPositionY() {
		return hitbox.getPositionY();
	}

	@Override
	public EngineService getEngine() {
		return engine;
	}

	@Override
	public HitboxService getCharBox() {
		return hitbox;
	}

	@Override
	public int getLife() {
		return life;
	}

	@Override
	public int getSpeed() {
		return speed;
	}
	
	@Override
	public boolean isJumping() {
		return isJumping;
	}
	
	@Override 
	public boolean isDown() {
		return isDown;
	}

	@Override
	public boolean faceRight() {
		return faceRight;
	}

	@Override
	public boolean dead() {
		return life <= 0;
	}

	// ----------------- Constructeur ----------------- //

	@Override
	public void init(int l, int s, boolean f, EngineService e) {
		engine = e;
		life = l;
		faceRight = f;
		speed = s;
		hitbox = new RectangleHitbox();
		// Engine va repositionner la hitobx du character
		((RectangleHitboxService)hitbox).init(-1, -1, 5, 2);
		isDown = false;
		isJumping = false;
		compteurJump = -1;
		hauteurNormale = ((RectangleHitboxService)hitbox).getHauteur();
		positionXNormale = getPositionX();
	}

	// ----------------- Operateurs ----------------- //

	@Override
	public void moveLeft() {
		if ( !isDown && !isJumping ) {
			int newX = getPositionX() - speed;
			HitboxService h = new RectangleHitbox();
			((RectangleHitboxService)h).init(newX, hitbox.getPositionY(), 
					((RectangleHitboxService)hitbox).getHauteur(), 
					((RectangleHitboxService)hitbox).getLargeur());

			// S'il n'y a pas de collision avec la hitbox de l'autre joueur
			if ( !engine.getChar(((monIndice(engine)+2) % 2)+1).getCharBox().collidesWith(h)) {
				if ( getPositionX() >= speed ) {
					hitbox.moveTo(newX, getPositionY());
				} else {
					hitbox.moveTo(0, getPositionY());
				}
			}
		}
	}

	@Override
	public void moveRight() {
		if ( !isDown && !isJumping ) {
			int newX = getPositionX() + speed;
			HitboxService h = new RectangleHitbox();
			((RectangleHitboxService)h).init(newX, hitbox.getPositionY(), 
					((RectangleHitboxService)hitbox).getHauteur(), 
					((RectangleHitboxService)hitbox).getLargeur());
			// S'il n'y a pas de collision avec la hitbox de l'autre joueur
			if ( !h.collidesWith(engine.getChar(((monIndice(engine)+2) % 2)+1).getCharBox()) ) {
				int largeurHitbox = ((RectangleHitboxService)hitbox).getLargeur();
				if ( newX + largeurHitbox >= engine.getWidth() ) {
					hitbox.moveTo(engine.getWidth() - largeurHitbox, getPositionY());
				} else {
					hitbox.moveTo(newX, getPositionY());
				}
			} 
		}
	}

	@Override
	public void switchSide() {
		faceRight = !faceRight;
	}

	@Override
	public void step(Commande c) {
		if(c == Commande.LEFT)
			moveLeft();
		if(c == Commande.RIGHT)
			moveRight();
		if(c == Commande.DOWN)
			moveDown();
		if(c == Commande.UP)
			moveTop();

		updateJump();
	}

	@Override
	public void moveDown() {
		if (!isJumping) {
			if(!isDown) {
				int h = ((RectangleHitboxService)hitbox).getHauteur();
				int hauteur = Math.round(5*h/7);
				((RectangleHitboxService)hitbox).init(hitbox.getPositionX(), 
						hitbox.getPositionY(), hauteur, 
						((RectangleHitboxService)hitbox).getLargeur());
				isDown = true;

			} else {
				((RectangleHitboxService)hitbox).init(hitbox.getPositionX(), 
						hitbox.getPositionY(), hauteurNormale, 
						((RectangleHitboxService)hitbox).getLargeur());
				isDown = false;
			}
		}
	}

	@Override
	public void moveTop() {
		if ( !isJumping && !isDown) {
			compteurJump = 0;
			isJumping = true;
		}
	}

	@Override
	public void getInjured(int damage) {
		life = life - damage;
	}

	// ----------------- Methodes pratiques ----------------- // 

	protected int monIndice(EngineService e) {
		for(int i = 0; i < 2; i++) {
			if( e.getChar(i+1).equals(this) )
				return i+1;
		}
		return -1;
	}

	protected void updateJump() {
		if ( isJumping ) {
			if(compteurJump >= jumpFrame ) {
				isJumping = false;
				compteurJump = -1;
			} else {
				if(compteurJump < hauteurJump ) {
					((RectangleHitboxService)hitbox).init(hitbox.getPositionX(), 
							hitbox.getPositionY()+1,  
							((RectangleHitboxService)hitbox).getHauteur(),
							((RectangleHitboxService)hitbox).getLargeur());
				} else {
					((RectangleHitboxService)hitbox).init(hitbox.getPositionX(), 
							hitbox.getPositionY()-1,  
							((RectangleHitboxService)hitbox).getHauteur(),
							((RectangleHitboxService)hitbox).getLargeur());
				}
			}
			compteurJump++;
		}
	}
}
