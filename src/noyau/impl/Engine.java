package noyau.impl;

import noyau.commande.Commande;
import noyau.services.CharacterService;
import noyau.services.EngineService;

public class Engine implements EngineService {
	private int height;
	private int width;
	private CharacterService[] charac;


	// ----------------- Accesseurs / Observateurs ----------------- //

	@Override
	public int getHeight() {
		return height;
	}
	@Override
	public int getWidth() {
		return width;
	}
	@Override
	public CharacterService getChar(int i) {
		return charac[i-1];
	}
	@Override
	public CharacterService getPlayer(int i) {
		return charac[i-1];
	}
	@Override
	public boolean gameOver() {
		return charac[0].dead() || charac[1].dead();
	}

	// ----------------- Constructeur ----------------- //

	@Override
	public void init(int h, int w, int s, CharacterService p1,
			CharacterService p2) {
		height = h; width = w;  
		charac = new CharacterService[2];
		charac[0] = p1;
		charac[1] = p2;

		try {
			// Engine repositionne les characters 
			charac[0].getCharBox().moveTo(Math.round(w/2 - s/2), 0);
			charac[1].getCharBox().moveTo(Math.round(w/2 + s/2), 0);
			
			System.out.println("Init Char 1 : "+charac[0].getPositionX()+","+charac[0].getPositionY());
			System.out.println("Init Char 2 : "+charac[1].getPositionX()+","+charac[1].getPositionY());

			if ( !charac[0].faceRight() )
				charac[0].switchSide();

			if ( charac[1].faceRight() )
				charac[1].switchSide();
		} catch(NullPointerException e) {
			System.err.println("Attention : Character null");
		}
	}

	// ----------------- Operateurs ----------------- //

	@Override
	public void step(Commande c1, Commande c2) {
		charac[0].step(c1);
		charac[1].step(c2);
	}

}
