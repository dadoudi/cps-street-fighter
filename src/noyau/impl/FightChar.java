package noyau.impl;

import noyau.commande.Commande;
import noyau.services.CharacterService;
import noyau.services.EngineService;
import noyau.services.FightCharService;
import noyau.services.RectangleHitboxService;
import noyau.services.TechData;

public class FightChar extends Character implements FightCharService {
	protected boolean blocking;
	protected int opponentStun; // le temps de stun de la technique adverse 
	protected int blockStunned;
	protected int hitStunned;
	protected boolean teching; // pas necessaire mais pratique
	protected boolean techHasAlreadyHit;
	protected TechData tech;
	protected int techFrame;

	// ----------------- Accesseurs / Observateurs ----------------- //
	@Override
	public boolean isBlocking() {
		return blocking;
	}

	@Override
	public boolean isBlockstunned() {
		return blockStunned >= 0;
	}

	@Override
	public boolean isHitstunned() {
		return hitStunned >= 0;
	}

	@Override
	public boolean isTeching() {
		return teching; 
	}

	@Override
	public TechData getTech() {
		return tech;
	}

	@Override
	public int techFrame() {
		return techFrame;
	}

	@Override
	public boolean techHasAlreadyHit() {
		// Un adversaire ne peut etre touché qu'une seule fois par une technique 
		return techHasAlreadyHit;
	}

	// ----------------- Constructeur ----------------- //

	public void init(int l, int s, boolean f, EngineService e) {
		super.init(l, s, f, e);
		blocking = false;
		opponentStun = -1;
		blockStunned = -1;
		hitStunned = -1;
		teching = false;
		tech = null;
		techFrame = -1;
		techHasAlreadyHit = false;
	}

	// ----------------- Operateurs ----------------- //

	@Override
	public void startTech(TechData tech){
		if( !teching && !( isBlockstunned() || isHitstunned() )) {
			teching = true;
			this.tech = tech;
			blocking = false;
			techFrame = 0;
		}		
		System.out.println("start, teching = "+teching);
	}

	@Override
	public void setBlocking(boolean b) {
		if(!teching)
			blocking = b;
	}

	@Override
	public void moveLeft() {
		if ( !teching && !( isBlockstunned() || isHitstunned() )) {
			super.moveLeft();
		}
	}

	@Override
	public void moveRight() {
		if ( !teching && !( isBlockstunned() || isHitstunned() )) {
			super.moveRight();
		}
	}

	@Override
	public void moveDown() {
		if ( !teching && !( isBlockstunned() || isHitstunned() )) {
			super.moveDown();
		}
	}

	@Override
	public void step(Commande c) {
		super.step(c);

		// On recupere l'autre joueur 
		int ind = monIndice(engine);
		int indAutreJoueur = ((ind+2) % 2)+1;
		CharacterService adversaire = engine.getChar(indAutreJoueur);

		// Si le personnage est en train de lancer une technique 
		if ( teching ) {
			// Si le personnage est dans une phase de hit et que sa 
			// technique n'a pas encore touché l'adversaire 
			if ( isHitting() && !techHasAlreadyHit()) {
				//System.out.println("step : "+teching);				
				// Placer la hitbox de la technique par rapport au personnage
				// y : hauteur 2/3 
				int yTech = Math.round(tech.ratio * (getPositionY() + ((RectangleHitboxService)hitbox).getHauteur()) / 4);
				// x : depend de faceRight 
				int xTech;
				if( faceRight ) {
					xTech = getPositionX() + ((RectangleHitboxService)hitbox).getLargeur();
				} else {
					xTech = getPositionX() - ((RectangleHitboxService)tech.hitbox).getLargeur();
				}
				tech.hitbox.moveTo(xTech, yTech);

				// Si la hitbox de la technique touche l'adversaire 
				if ( tech.hitbox.collidesWith(adversaire.getCharBox())) {

					if( ((FightCharService)adversaire).isBlocking() ) {
						// Si l'adversaire se protege => block stun
						((FightCharService)adversaire).getStunned(tech.blockstun);
					} else {
						// Si l'adversaire ne se protege pas => hit stun + degats 
						
						// Extension : Si l'adversaire lançait une technique, la technique
						// fait plus de degat et hitstun 
						int damage;
						int stun;
						if( ((FightCharService)adversaire).isTeching() ) {
							damage = tech.damage+1;
							stun = tech.hitstun+2;
						} else {
							damage = tech.damage;
							stun = tech.hitstun;
						}
						((FightCharService)adversaire).getStunned(stun);
						adversaire.getInjured(damage);
					}
					techHasAlreadyHit = true;
				}
			}
		}

		// mise a jour des compteurs lies à tech et stun 

		updateTeching();
		updateStunned();
	}


	@Override
	public void getStunned(int stunTime) {
		// Si le personnage est touché pendant qu'il exécute une technique, 
		// et qu'il n'a toujours pas touché son adversaire avec sa tech,
		//  alors la tech est interrompue 
		if( teching && !isRecovering() && !techHasAlreadyHit) 
			interruptTech();
		if ( isBlocking() ) {
			blockStunned = 0;
		} else {
			if( isBlockstunned() || isHitstunned() ) {
				// Si le personnage etait stun : interrompre et reprendre à 0
				blockStunned = -1;
				hitStunned = -1;
			}
			hitStunned = 0;
		}
		opponentStun = stunTime; 
	}

	// ----------------- Methodes pratiques ----------------- //

	protected void interruptTech() {
		// Permet de reinitialiser toutes les variables liees a une technique
		tech = null;
		teching = false;
		techHasAlreadyHit = false;
		techFrame = -1;
	}

	protected void updateStunned() {
		if ( isBlockstunned() ) {
			if ( blockStunned >= opponentStun ) {
				// Si le temps de stun est ecoule
				blockStunned = -1;
				opponentStun = -1;
			} else {
				blockStunned++;
				System.out.println("Blockstun = "+blockStunned);
			}
		}
		if ( isHitstunned() ) {
			if ( hitStunned >= opponentStun ) {
				// Si le temps de stun est ecoule
				hitStunned = -1;
				opponentStun = -1;
			} else {
				hitStunned++;
			}
		}
	}

	protected void updateTeching() {
		if(teching) {
			// Si le personnage a fini sa technique
			if ( !isStarting() && !isHitting() && !isRecovering() ) {
				interruptTech();
			} else {
				// Si la technique n'est pas finie, on incremente le compteur 
				techFrame++;
			}
		}
	}

	protected boolean isStarting() {
		return techFrame >= 0 && techFrame <= tech.startupframe;
	}

	protected boolean isHitting() {
		return techFrame > tech.startupframe  
				&& techFrame <= tech.startupframe + tech.hitframe;
	}

	protected boolean isRecovering() {
		return techFrame > tech.startupframe + tech.hitframe 
				&& techFrame <= tech.startupframe + tech.hitframe + tech.recoveryframe;
	}

}
