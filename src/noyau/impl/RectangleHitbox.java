package noyau.impl;

import noyau.services.HitboxService;
import noyau.services.RectangleHitboxService;

public class RectangleHitbox extends Hitbox implements RectangleHitboxService  {
	private int hauteur;
	private int largeur;


	@Override
	public boolean belongsTo(int i, int j) {
		return i < positionX + largeur && j < positionY 
				+ hauteur && i >= positionX && j >= positionY  ;
	}

	@Override
	public boolean collidesWith(HitboxService H1) {
		for(int x = positionX; x < positionX + largeur; x++){
			for(int y = positionY; y < positionY + hauteur; y++){
				if(H1.belongsTo(x,y)){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean equalsTo(HitboxService H1) {
		boolean equals = true;
		for(int x = 0; x<getLargeur();x++){
			for(int y = 0; y <getHauteur(); y++){
				if( (belongsTo(x,y) && ! belongsTo(x,y))  || ( !belongsTo(x,y) &&  belongsTo(x,y)) ){
					equals = false; 
				}
			}
		}
		return equals;
	}

	@Override
	public void init(int x, int y) {
		init(x,y,10,5);
	}

	@Override
	public int getHauteur() {
		return hauteur;
	}

	@Override
	public int getLargeur() {
		return largeur;
	}

	@Override
	public void init(int x, int y, int h, int l) {
		super.init(x, y);
		hauteur = h;
		largeur = l; 
	}

}
