package noyau.impl;

import noyau.services.HitboxService;

// Hitbox classe abstraite
public abstract class Hitbox implements HitboxService {

	protected int positionX;
	protected int positionY;

	
	@Override
	public int getPositionX() {
		return positionX;
	}

	@Override
	public int getPositionY() {
		return positionY;
	}
	@Override
	public boolean belongsTo(int i, int j) {
		return false;
	}

	@Override
	public boolean collidesWith(HitboxService H1) {
		return false;
	}

	@Override
	public boolean equalsTo(HitboxService H1) {
		return false;
	}

	@Override
	public void init(int x, int y) {
		positionX = x;
		positionY = y;
	}

	@Override
	public void moveTo(int x, int y) {
		positionX = x;
		positionY = y;
	}

	
}
