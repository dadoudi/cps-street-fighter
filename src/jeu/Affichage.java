package jeu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;

import jeu.factory.TechsFactory;
import noyau.commande.Commande;
import noyau.impl.*;
import noyau.impl.Character;
import noyau.services.CharacterService;
import noyau.services.EngineService;
import noyau.services.FightCharService;
import noyau.services.HitboxService;
import noyau.services.RectangleHitboxService;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.SystemColor;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.Font;

public class Affichage extends JPanel implements KeyListener, Runnable {
	private static final long serialVersionUID = 1L;

	/* Jeu */
	private EngineService engine;
	private Commande charOne;
	private Commande charTwo;
	private CharacterService studentOne;
	private CharacterService studentTwo;

	/* Afichage */
	public JFrame frame;
	private JProgressBar lifeP1;
	private JProgressBar lifeP2;
	private JLabel blocking1;
	private JLabel blocking2;
	private int spriteLength = 50;
	private BufferedImage PlayerOne;
	private BufferedImage PlayerTwo;
	private BufferedImage PlayerOneTeching;
	private BufferedImage PlayerTwoTeching;
	private JLabel background;
	private JLabel streer_fighter_logo;
	JLabel Ryu;


	public Affichage() {
		super();
		setBackground(Color.LIGHT_GRAY);
		initCommandes();
		initEngine();
		
		// Init  Frame 
		setLayout(null);
		frame = new JFrame("Street Fighter");
		frame.getContentPane().setBackground(Color.GRAY);
		frame.getContentPane().add(this, BorderLayout.CENTER);
		frame.setSize(742, 296);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	
		addKeyListener(this);
		frame.addKeyListener(this);
		frame.requestFocus();
		frame.setBackground(Color.gray);
		setLayout(null);
		
		initAffichage();
	}
	
	private void initAffichage() {

		// Initialisation panel et frame
		/*setLayout(null);
		frame = new JFrame("Street Fighter");
		frame.getContentPane().setBackground(Color.GRAY);
		frame.getContentPane().add(this, BorderLayout.CENTER);
		frame.setSize(742, 296);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	
		addKeyListener(this);
		frame.addKeyListener(this);
		frame.requestFocus();
		frame.setBackground(Color.gray);
		setLayout(null);*/

		// Init Life Bars

		UIManager.put("ProgressBar.selectionBackground", Color.black);
		lifeP1 = new JProgressBar(0, studentOne.getLife());
		lifeP1.setVisible(false);
		
		Ryu = new JLabel("");
		Ryu.setIcon(new ImageIcon("C:\\Users\\Hajar\\Desktop\\ryu-sf2-stance.gif"));
		System.out.println();
		Ryu.setBounds(178, 147, 63, 110);
		Ryu.setVisible(false);
		add(Ryu);
		
		JLabel lblNewLabel = new JLabel("Player 1 wins");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setBounds(272, 78, 200, 136);
		add(lblNewLabel);
		
		lifeP1.setValue(studentOne.getLife());
		lifeP1.setString("PLAYER 1");
		lifeP1.setStringPainted(true);
		add(lifeP1);
		lifeP1.setBounds(0, 0, 368 , 47);
		lifeP1.setForeground(new Color(64, 131, 255));


		lifeP2 = new JProgressBar(0, studentTwo.getLife());
		lifeP2.setVisible(false);
		lifeP2.setValue(studentTwo.getLife());
		lifeP2.setString("PLAYER 2");
		lifeP2.setStringPainted(true);
		add(lifeP2);
		lifeP2.setBounds(376, 0, 362, 47);
		lifeP2.setForeground(new Color(234, 34, 69));

		// Init Labels 

		blocking1 = new JLabel("Blocking");
		blocking2 = new JLabel("Blocking");
		add(blocking1);
		add(blocking2);
		blocking1.setBounds(2, 46, 69,73);
		blocking2.setBounds(-129, 19, 200,100);
		
		background = new JLabel("");
		background.setVisible(false);
		background.setHorizontalTextPosition(SwingConstants.CENTER);
		background.setIcon(new ImageIcon("C:\\Users\\Hajar\\Desktop\\TYttEQo.gif"));
		background.setBounds(-2, 41, 740, 234);
		add(background);
		
		JButton btnNewButton = new JButton("PLAY !");
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 17));
		btnNewButton.setForeground(SystemColor.windowBorder);
		btnNewButton.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, SystemColor.textText));
		btnNewButton.setBackground(SystemColor.control);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lifeP1.setVisible(true);
				background.setVisible(true);
				//blocking1.setVisible(true);
				//blocking2.setVisible(true);
				lifeP2.setVisible(true);
				streer_fighter_logo.setVisible(false);
				btnNewButton.setVisible(false);
				Ryu.setVisible(true);
			}
		});
		btnNewButton.setBounds(283, 147, 172, 73);
		add(btnNewButton);
		btnNewButton.setActionCommand("Play !");
		
		streer_fighter_logo = new JLabel("");
		streer_fighter_logo.setIcon(new ImageIcon("C:\\Users\\Hajar\\Desktop\\Street_Fighter.png"));
		streer_fighter_logo.setBounds(227, 0, 277, 130);
		add(streer_fighter_logo);
		blocking1.setVisible(false);
		blocking2.setVisible(false);
		
		// Recuperation des sprites 
		try {
			PlayerOne = ImageIO.read(getClass().getResource("playerOne.png"));
			PlayerTwo = ImageIO.read(getClass().getResource("playerTwo.png"));
			PlayerOneTeching = ImageIO.read(getClass().getResource("PlayerOneTeching.png"));
			PlayerTwoTeching = ImageIO.read(getClass().getResource("PlayerTwoTeching.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initFighters() {
		studentOne = new FightChar();
		studentOne.init(20, 20, true, engine);

		studentTwo = new FightChar();
		studentTwo.init(20, 20, true, engine);
	}

	private void initCommandes() {
		charOne = Commande.NEUTRAL;
		charTwo = Commande.NEUTRAL;
	}

	private void initEngine() {
		engine = new Engine();
		initFighters();
		engine.init(296, 742, 300, studentOne, studentTwo);
	}

	@Override
	public void run() {
		
		
		while ( !engine.gameOver() ) {

			engine.step(charOne, charTwo);
			initCommandes();
			repaint();
			try {
				// 500 ideal
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void paintHitbox(BufferedImage img, Graphics g, HitboxService h) {
		// Au lieu de traiter toute la fenetre, on ne paint 
		// que les hitbox 
		int borneMaxX = ((RectangleHitboxService)h).getLargeur();
		int borneMaxY = ((RectangleHitboxService)h).getHauteur();
		int x = h.getPositionX();
		int y = h.getPositionY();

		for(int i = x; i < x + borneMaxX; i++) {
			for(int j = y; j < y + borneMaxY; j++) {
				g.drawImage(img, spriteLength * i, spriteLength
						* (engine.getHeight()-1 - j), 
						spriteLength, spriteLength, frame);
			}
		}
	}

	private void paintTech(FightCharService fighter, Graphics g, BufferedImage img, BufferedImage imgTch) {
		// Si le personnage est en phase de hit 
		if(fighter.isTeching() ) {
			if( fighter.techFrame() > fighter.getTech().startupframe
					&& fighter.techFrame() <= fighter.getTech().startupframe + fighter.getTech().hitframe) {
				paintHitbox(imgTch, g, fighter.getCharBox());
				paintHitbox(imgTch, g, fighter.getTech().hitbox);
			} else {
				paintHitbox(img, g, fighter.getCharBox());
			}
		} else {
			paintHitbox(img, g, fighter.getCharBox());
		}
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		super.paint(g2);
		HitboxService hitboxOne = studentOne.getCharBox();
		HitboxService hitboxTwo = studentTwo.getCharBox();

		if ( studentOne instanceof FightCharService ) {
			paintTech(((FightCharService)studentOne),g,PlayerOne,PlayerOneTeching);

			// Etat : Blocking 
			if(((FightCharService) studentOne).isBlocking()) {
				blocking1.setVisible(true);
			} else {
				blocking1.setVisible(false);
			}
		} else {
			paintHitbox(PlayerOne, g2, hitboxOne);
		}

		if ( studentTwo instanceof FightCharService ) {
			paintTech(((FightCharService)studentTwo),g,PlayerTwo,PlayerTwoTeching);

			// Etat : Blocking 
			if(((FightCharService) studentTwo).isBlocking()) {
				blocking2.setVisible(true);
			} else {
				blocking2.setVisible(false);			}
		} else {
			paintHitbox(PlayerTwo, g2, hitboxTwo);
		}


		// Mise a jour des barres de vie

		lifeP1.setValue(studentOne.getLife());
		lifeP2.setValue(studentTwo.getLife());

	}

	public static void main(String[] args) {
		(new Thread(new Affichage())).start();
	}

	// ----------------- Key Listener ----------------- //

	@Override
	public void keyTyped(KeyEvent e) {
		// Rien 
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// Pour le moment les personnages ne peuvent pas sauter ou s'accroupir 

		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			charTwo = Commande.NEUTRAL;
			break;
		case KeyEvent.VK_RIGHT:
			charTwo = Commande.RIGHT;
			break;
		case KeyEvent.VK_DOWN:
			charTwo = Commande.NEUTRAL;
			break;
		case KeyEvent.VK_LEFT:
			charTwo = Commande.LEFT;
			break;
		case KeyEvent.VK_Z:
			charOne = Commande.NEUTRAL;
			break;
		case KeyEvent.VK_Q:
			charOne = Commande.LEFT;
			break;
		case KeyEvent.VK_S:
			charOne = Commande.NEUTRAL;
			break;
		case KeyEvent.VK_D:
			charOne = Commande.RIGHT;
			break;

			// Blocking Joueur 1
		case KeyEvent.VK_A:
			// Si le personnage se protegeait deja => il ne se protege plus
			if(studentOne instanceof FightCharService) {
				((FightCharService)studentOne).setBlocking(!(((FightCharService)studentOne).isBlocking()));
			}
			break;
			// Blocking Joueur 2
		case KeyEvent.VK_M:
			System.out.println("Ici ");
			if(studentTwo instanceof FightCharService) {
				((FightCharService)studentTwo).setBlocking(!(((FightCharService)studentTwo).isBlocking()));
			}
			break;

			// Techniques 

			// Technique 1 Joueur 1 */
		case KeyEvent.VK_E:
			if(studentOne instanceof FightCharService) {
				System.out.println("here");
				((FightCharService) studentOne).startTech(TechsFactory.coupDePoing());
			}
			break;
		}		

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// Rien
	}
}
