package jeu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;

import jeu.factory.TechsFactory;
import noyau.commande.Commande;
import noyau.impl.*;
import noyau.services.CharacterService;
import noyau.services.EngineService;
import noyau.services.FightCharService;
import noyau.services.HitboxService;
import noyau.services.RectangleHitboxService;

public class StreetFighter extends JPanel implements KeyListener, Runnable {
	private static final long serialVersionUID = 1L;

	/* Jeu */
	private EngineService engine;
	private Commande charOne;
	private Commande charTwo;
	private CharacterService studentOne;
	private CharacterService studentTwo;

	/* Afiichage */
	public JFrame frame;
	private JProgressBar lifeP1;
	private JProgressBar lifeP2;
	private JLabel blocking1;
	private JLabel blocking2;
	private JLabel isTeching1;
	private JLabel isTeching2;
	private int spriteLength = 50;
	private BufferedImage PlayerOne;
	private BufferedImage PlayerTwo;
	private BufferedImage PlayerOneTeching;
	private BufferedImage PlayerTwoTeching;


	public StreetFighter() {
		super();
		initCommandes();
		initEngine();
		initAffichage();
	}

	private void initAffichage() {

		// Initialisation panel et frame

		frame = new JFrame("Street Fighter (not the real one lel)");
		frame.getContentPane().add(this);
		frame.setSize(engine.getWidth()*spriteLength, 
				engine.getHeight()*spriteLength);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	
		addKeyListener(this);
		frame.addKeyListener(this);
		frame.requestFocus();
		frame.setBackground(Color.gray);
		setLayout(null);

		// Init Life Bars

		UIManager.put("ProgressBar.selectionBackground", Color.black);
		lifeP1 = new JProgressBar(0, studentOne.getLife());
		lifeP1.setVisible(true);
		lifeP1.setValue(studentOne.getLife());
		lifeP1.setString("PLAYER 1");
		lifeP1.setStringPainted(true);
		add(lifeP1);
		int mid = engine.getWidth() / 2;
		lifeP1.setBounds((int)(spriteLength*0.3), (int)(spriteLength*0.3), spriteLength*mid , spriteLength*1);
		lifeP1.setForeground(new Color(64, 131, 255));


		lifeP2 = new JProgressBar(0, studentTwo.getLife());
		lifeP2.setVisible(true);
		lifeP2.setValue(studentTwo.getLife());
		lifeP2.setString("PLAYER 2");
		lifeP2.setStringPainted(true);
		add(lifeP2);
		lifeP2.setBounds(engine.getWidth()*spriteLength - mid*spriteLength - spriteLength/2, 
				(int)(spriteLength*0.3), spriteLength*mid, spriteLength*1);
		lifeP2.setForeground(new Color(234, 34, 69));

		// Init Labels 

		blocking1 = new JLabel("Blocking");
		blocking2 = new JLabel("Blocking");
		add(blocking1);
		add(blocking2);
		blocking1.setBounds((int)(spriteLength*0.3)+10, (int)(spriteLength*0.3)+15, 200,100);
		blocking2.setBounds(engine.getWidth()*spriteLength - (int)(spriteLength*1.7), (int)(spriteLength*0.3)+15, 200,100);
		blocking1.setVisible(false);
		blocking2.setVisible(false);

		isTeching1 = new JLabel("Teching");
		isTeching1.setSize(97, 20);
		isTeching1.setLocation(300, 72);
		isTeching1.setVisible(false);

		isTeching2 = new JLabel("Teching");
		isTeching2.setSize(97, 20);
		isTeching2.setLocation(380, 72);
		isTeching2.setVisible(false);

		add(isTeching1); add(isTeching2);



		// Recuperation des sprites 
		try {
			PlayerOne = ImageIO.read(getClass().getResource("playerOne.png"));
			PlayerTwo = ImageIO.read(getClass().getResource("playerTwo.png"));
			PlayerOneTeching = ImageIO.read(getClass().getResource("PlayerOneTeching.png"));
			PlayerTwoTeching = ImageIO.read(getClass().getResource("PlayerTwoTeching.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initFighters() {
		studentOne = new FightChar();
		studentOne.init(20, 1, true, engine);

		studentTwo = new FightChar();
		studentTwo.init(20, 1, true, engine);
	}

	private void initCommandes() {
		charOne = Commande.NEUTRAL;
		charTwo = Commande.NEUTRAL;
	}

	private void initEngine() {
		engine = new Engine();
		initFighters();
		engine.init(9, 15, 2, studentOne, studentTwo);
	}

	@Override
	public void run() {
		while ( !engine.gameOver() ) {

			engine.step(charOne, charTwo);
			initCommandes();
			repaint();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		JLabel win;
		if(studentOne.dead()) {
			win = new JLabel("Player 2 wins");
		} else {
			win = new JLabel("Player 1 wins");
		}
		win.setFont(new Font("Tahoma", Font.PLAIN, 30));
		win.setBounds(272, 78, 200, 136);
		add(win);
		frame.add(win);
		win.setVisible(true);
	}

	private void paintHitbox(BufferedImage img, Graphics g, HitboxService h) {
		// Au lieu de traiter toute la fenetre, on ne paint 
		// que les hitbox 
		int borneMaxX = ((RectangleHitboxService)h).getLargeur();
		int borneMaxY = ((RectangleHitboxService)h).getHauteur();
		int x = h.getPositionX();
		int y = h.getPositionY();

		for(int i = x; i < x + borneMaxX; i++) {
			for(int j = y; j < y + borneMaxY; j++) {
				g.drawImage(img, spriteLength * i, spriteLength
						* (engine.getHeight()-1 - j), 
						spriteLength, spriteLength, frame);
			}
		}
	}

	private void paintTech(FightCharService fighter, Graphics g, BufferedImage img, BufferedImage imgTch) {
		// Si le personnage est en phase de hit 
		if(fighter.isTeching() ) {
			if( fighter.techFrame() > fighter.getTech().startupframe
					&& fighter.techFrame() <= fighter.getTech().startupframe + fighter.getTech().hitframe) {
				paintHitbox(imgTch, g, fighter.getCharBox());
				paintHitbox(imgTch, g, fighter.getTech().hitbox);
			} else {
				paintHitbox(img, g, fighter.getCharBox());
			}
		} else {
			paintHitbox(img, g, fighter.getCharBox());
		}
	}

	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		super.paint(g2);
		HitboxService hitboxOne = studentOne.getCharBox();
		HitboxService hitboxTwo = studentTwo.getCharBox();

		if ( studentOne instanceof FightCharService ) {
			paintTech(((FightCharService)studentOne),g,PlayerOne,PlayerOneTeching);

			// Etat : Blocking 
			if(((FightCharService) studentOne).isBlocking()) {
				blocking1.setVisible(true);
			} else {
				blocking1.setVisible(false);
			}

			// Teching 
			if(((FightCharService) studentOne).isTeching()) {
				isTeching1.setVisible(true);
			} else {
				isTeching1.setVisible(false);
			}

		} else {
			paintHitbox(PlayerOne, g2, hitboxOne);
		}

		if ( studentTwo instanceof FightCharService ) {
			paintTech(((FightCharService)studentTwo),g,PlayerTwo,PlayerTwoTeching);

			// Etat : Blocking 
			if(((FightCharService) studentTwo).isBlocking()) {
				blocking2.setVisible(true);
			} else {
				blocking2.setVisible(false);			
			}

			// Teching 
			if(((FightCharService) studentTwo).isTeching()) {
				isTeching2.setVisible(true);
			} else {
				isTeching2.setVisible(false);
			}
		} else {
			paintHitbox(PlayerTwo, g2, hitboxTwo);
		}


		// Mise a jour des barres de vie

		lifeP1.setValue(studentOne.getLife());
		lifeP2.setValue(studentTwo.getLife());

	}

	public static void main(String[] args) {
		(new Thread(new StreetFighter())).start();
	}

	// ----------------- Key Listener ----------------- //

	@Override
	public void keyTyped(KeyEvent e) {
		// Rien 
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// Pour le moment les personnages ne peuvent pas sauter ou s'accroupir 

		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			charTwo = Commande.UP;
			break;
		case KeyEvent.VK_RIGHT:
			charTwo = Commande.RIGHT;
			break;
		case KeyEvent.VK_DOWN:
			charTwo = Commande.DOWN;
			break;
		case KeyEvent.VK_LEFT:
			charTwo = Commande.LEFT;
			break;
		case KeyEvent.VK_Z:
			charOne = Commande.UP;
			break;
		case KeyEvent.VK_Q:
			charOne = Commande.LEFT;
			break;
		case KeyEvent.VK_S:
			charOne = Commande.DOWN;
			break;
		case KeyEvent.VK_D:
			charOne = Commande.RIGHT;
			break;

			// Blocking Joueur 1
		case KeyEvent.VK_A:
			// Si le personnage se protegeait deja => il ne se protege plus
			if(studentOne instanceof FightCharService) {
				((FightCharService)studentOne).setBlocking(!(((FightCharService)studentOne).isBlocking()));
			}
			break;
			// Blocking Joueur 2
		case KeyEvent.VK_M:
			if(studentTwo instanceof FightCharService) {
				((FightCharService)studentTwo).setBlocking(!(((FightCharService)studentTwo).isBlocking()));
			}
			break;

			// Techniques 

			/* Coup de poing */
			/* Joueur 1 */
		case KeyEvent.VK_E:
			if(studentOne instanceof FightCharService) {
				((FightCharService) studentOne).startTech(TechsFactory.coupDePoing());
			}
			break;
			/* Joueur 2 */
		case KeyEvent.VK_L:
			if(studentTwo instanceof FightCharService) {
				((FightCharService) studentTwo).startTech(TechsFactory.coupDePoing());
			}
			break;
			
			/* Coup de pied */
		case KeyEvent.VK_R:
			if(studentOne instanceof FightCharService) {
				((FightCharService) studentOne).startTech(TechsFactory.coupDePied());
			}
			break;
			/* Joueur 2 */
		case KeyEvent.VK_K:
			if(studentTwo instanceof FightCharService) {
				((FightCharService) studentTwo).startTech(TechsFactory.coupDePied());
			}
			break;
		}		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// Rien
	}
}
