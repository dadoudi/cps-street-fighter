package jeu.factory;

import noyau.impl.RectangleHitbox;
import noyau.services.HitboxService;
import noyau.services.TechData;

public abstract class TechsFactory {
	
	public static TechData coupDePoing() {
		HitboxService hitbox = new RectangleHitbox();
		((RectangleHitbox)hitbox).init(-1, -1, 1, 1);
		TechData t = new TechData(3, 5, 1, 2, 3, 3, hitbox,3);
		return t;
	}
	
	public static TechData coupDePied() {
		HitboxService hitbox = new RectangleHitbox();
		((RectangleHitbox)hitbox).init(-1, -1, 1, 2);
		TechData t = new TechData(4, 3, 1, 4, 4, 4, hitbox,2);
		return t;
	}

}
